// CSBridge - Bridge from D/C++ to C#
//
// see makelib.bat for notes and how to compile this

#using "CSCode.exe"
using namespace System;

UInt64 csCheck() {
        try { return CSCode::CSBridge::CSCheck(); }
        catch (...) { return 101010; }
}

UInt64 csFree() {
        return CSCode::CSBridge::CSFree();
}

UInt64 csLength() {
        return CSCode::CSBridge::CSLength();
}

UInt64 csAlloc(UInt64 arg) {
        return CSCode::CSBridge::CSAlloc(arg);
}

UInt64 csExec(UInt64 arg) {
        return CSCode::CSBridge::CSExec(arg);
}
