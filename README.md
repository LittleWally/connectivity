# Connectivity

## D to C# (legacy .Net) connectivity examples

#### D to C# via C++/CLI

https://gitlab.com/LittleWally/connectivity/-/tree/main/CLI

#### D to C# via Unmanaged Exports

https://sites.google.com/site/robertgiesecke/Home/uploads/unmanagedexports

#### D to C# via Mono

#### D to C# Marshaling (avoid Dueling Garbage Collectors)
